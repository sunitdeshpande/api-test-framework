package com.example.apiframework.steps;

import com.example.apiframework.data.ArticleApiDataProvider;
import com.example.apiframework.models.Article;
import com.example.apiframework.services.ArticleApiService;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class ArticleApiSteps {

    private final ArticleApiService articleApiService;
    private final ArticleApiDataProvider articleApiDataProvider;

    // TODO: Use dependency injection
    public ArticleApiSteps() {
        this.articleApiService = new ArticleApiService();
        this.articleApiDataProvider = new ArticleApiDataProvider();
    }

    @When("I hit the webservice at article get detail url with id {int}")
    public void whenIHitTheWebserviceAtArticleGetDetailUrlWithId(int articleId) throws Exception {
        articleApiService.requestArticleDetail(articleId);
    }

    @Then("The status code should be {int}")
    public void theStatusCodeShouldBe(int statusCode) {
        articleApiService.assertLastStatusCode(statusCode);
    }

    @And("The response body should match first article")
    public void theResponseShouldMatchFirstArticle() {
        Article article = articleApiService.getArticle();
        assertEquals(article, articleApiDataProvider.getFirstArticle());
    }

    @When("I hit the webservice at article list url")
    public void iHitTheWebserviceAtArticleListUrl() throws Exception {
        articleApiService.requestArticlesList();
    }

    @And("There are {int} items in the response")
    public void thereAreItemsInTheResponse(int count) {
        List<Article> articleList = articleApiService.getArticles();
        assertEquals(articleList.size(), count);
     }

    @And("The response body should match the initial article list")
    public void theResponseBodyShouldMatchTheInitialArialList() throws Exception {
        List<Article> articleList = articleApiService.getArticles();
        assertThat(articleList, is(articleApiDataProvider.getInitialArticles()));
    }

}
