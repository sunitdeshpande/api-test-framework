package com.example.apiframework.data;

import com.example.apiframework.models.Article;
import com.example.apiframework.utils.ResourceUtils;
import com.google.gson.reflect.TypeToken;

import java.net.URISyntaxException;
import java.util.List;

public class ArticleApiDataProvider {

    private final String ARTICLE_LIST_RESPONSE_JSON_PATH = "responses/articles/article_list_response.json";

    public Article getFirstArticle() {

        // TODO: Replace this with builder pattern
        Article article = new Article();
        article.setId(1);
        article.setUserId(1);
        article.setTitle("sunt aut facere repellat provident occaecati excepturi optio reprehenderit");
        article.setBody("quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto");

        return article;
    }

    public List<Article> getInitialArticles() throws Exception {
        return ResourceUtils.getJsonResourceAsObject(ARTICLE_LIST_RESPONSE_JSON_PATH, new TypeToken<List<Article>>() {}.getType());
    }
}
