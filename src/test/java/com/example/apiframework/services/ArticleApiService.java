package com.example.apiframework.services;

import com.example.apiframework.models.Article;
import io.restassured.response.Response;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;

/**
 * Service for post api at // https://jsonplaceholder.typicode.com/
 */
public class ArticleApiService {
     private final String POSTS_URL = "/posts";

     private Response response;

     // TODO: Proper way then throwing Exception class
     public void requestArticleDetail(int articleId) throws Exception {

          final URI articleDetailUrl = new URI(POSTS_URL + "/" + articleId);

          response = given().get(articleDetailUrl);
     }

     // TODO: Proper way then throwing Exception class
     public void requestArticlesList() throws Exception {

          final URI articleListUrl = new URI(POSTS_URL);

          response = given().get(articleListUrl);
     }

     public void assertLastStatusCode(int statusCode){
          response.then().statusCode(statusCode);
     }

     public Article getArticle(){
          return response.as(Article.class);
     }

     public List<Article> getArticles(){
          return Arrays.asList(response.as(Article[].class));
     }

}
