package com.example.apiframework;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber-reports", "json:target/report.json"},
        features = {"src/test/resources/features"}
)
public class TestRunner {

    private final static String BASE_URI = "https://jsonplaceholder.typicode.com/";

    @BeforeClass
    public static void setUp() {
        // Setup Rest Assured
        RestAssuredConfig restAssuredConfig = RestAssuredConfig.config()
                .objectMapperConfig(new ObjectMapperConfig(ObjectMapperType.GSON));

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setConfig(restAssuredConfig)
                .setContentType(ContentType.JSON)
                .setBaseUri(BASE_URI)
                .build();
    }
}
