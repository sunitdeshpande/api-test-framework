package com.example.apiframework.utils;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

public class ResourceUtils {

    public static <T> T getJsonResourceAsObject(String resourcePath, Type type) {
        InputStream inputStream = ResourceUtils.class.getClassLoader().getResourceAsStream(resourcePath);
        return new Gson().fromJson(new InputStreamReader(inputStream), type);
    }

    public static <T> T getJsonResourceAsObject(String resourcePath, Class<T> objectClass) {
        InputStream inputStream = ResourceUtils.class.getClassLoader().getResourceAsStream(resourcePath);
        return new Gson().fromJson(new InputStreamReader(inputStream), objectClass);
    }
}
