package com.example.apiframework.models;

import lombok.Data;

@Data
public class Article {
    private int id;
    private int userId;
    private String title;
    private String body;
}
