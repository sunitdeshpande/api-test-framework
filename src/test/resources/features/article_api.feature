Feature: Article api

  Scenario: Get a single article by id
    When I hit the webservice at article get detail url with id 1
    Then The status code should be 200
    And The response body should match first article

  Scenario: Get list of articles
    When I hit the webservice at article list url
    Then The status code should be 200
    And There are 100 items in the response
    And The response body should match the initial article list